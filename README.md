If your purpose is to use this web application to calculate control times:
--------------------------------------------------------------------------
This web application takes a total distance, a beginning time and date, and will require the user to input a distance. The user can pick either miles or kilometers to enter, and the program will calculate an open and closing time based on the input. Although it is easier to use miles, as they are required to be whole numbers. The user can submit control times to be saved by hitting the submit button at the bottom of the page, and once they are saved they can be displayed by hitting the display button, also at the bottom of the page.

If the user enters in a time greater than the max brevet, or the brevet chosen, it will calculate a time based on the max brevet, or the brevet chosen respectively

To find the calculation for how the control times are calculated: https://rusa.org/pages/acp-brevet-control-times-calculator

To run examples: https://rusa.org/octime_acp.html

If your purpose is to further develop this application:
-------------------------------------------------------

The web application uses AJAX and Flask to display the open and closing times on the webpage. The control time calculation is done within acp_times.py, which uses table driven design to calculate this for further versatility. The credentials.ini file needs to be filled in with the port, author and the git repository for your further developed calculator. The program is ran on docker to account for dependencies, you may use the run.sh file to run the project on port 5000. For testing, you are able to follow the logic in testing.txt for manual tests. The Flask  and database logic is contained in app.py

Author: Wil Sprouse, wils@cs.uoregon.edu
